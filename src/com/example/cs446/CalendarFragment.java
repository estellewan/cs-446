package com.example.cs446;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.tyczj.extendedcalendarview.Day;
import com.tyczj.extendedcalendarview.ExtendedCalendarView;
import com.tyczj.extendedcalendarview.ExtendedCalendarView.OnDayClickListener;

public class CalendarFragment extends Fragment {
    
    OnDaySelectedListener mCallback;

    // Container Activity must implement this interface
    public interface OnDaySelectedListener {
        public void onDaySelected(int d);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, 
            ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_calendar, container, false);
        
        ExtendedCalendarView calendar = (ExtendedCalendarView)view.findViewById(R.id.calendar);
        
        getActivity().setTitle(R.string.title_activity_calendar);
        
        calendar.setOnDayClickListener(new OnDayClickListener() {
            
            @Override
            public void onDayClicked(AdapterView<?> adapter, View view, int position, long id, Day day) {
                // TODO Auto-generated method stub
                Calendar startDate = Calendar.getInstance();
                Calendar endDate = Calendar.getInstance(); 
                            
                endDate.set(day.getYear(), day.getMonth(), day.getDay());
                
                long millisDiff = endDate.getTimeInMillis() - startDate.getTimeInMillis();
                long relDay = millisDiff / (24 * 60 * 60 * 1000); 
                
                mCallback.onDaySelected((int)relDay);
            }
        } );
        
        return view;
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnDaySelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnDaySelectedListener");
        }
    }

}