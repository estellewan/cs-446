package com.example.cs446;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.cs446.adapters.BaseInflaterAdapter;
import com.example.cs446.adapters.CardItemData;
import com.example.cs446.adapters.inflaters.CardInflater;
import com.example.cs446.decorators.ListFragmentDecorator;
import com.example.cs446.interfaces.IFragment;
import com.example.cs446.models.EventDetail;


@SuppressLint("SimpleDateFormat")
public class EventFragment extends ListFragmentDecorator {

    List<EventDetail> eventList = new ArrayList<EventDetail>();
    
    private final static DateFormat df  = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ");
    
    OnEventSelectedListener mCallback;
    
    public EventFragment() {
        super(null);
        listFragment = new MainFragment();
    }
    /*
    public EventFragment(IFragment listFragment) {
        super(listFragment);
    }*/
    
    // Container Activity must implement this interface
    public interface OnEventSelectedListener {
        public void onEventSelected(int id, String site);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnEventSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnEventSelectedListener");
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, 
            ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_events, container, false);
        list = (ListView)view.findViewById(R.id.list_view);
        updateView(relDay);
        return view;
    }  

    
    @Override
    public void addListToAdapter() {
        super.addListToAdapter();
    }

    @Override
    protected void clearCardList() {
        super.clearCardList();
        eventList.clear();
    }
    
    @Override
    public void updateView(int fragmentId) {
        super.updateView(fragmentId);
    }
    
    @Override
    public String getAPIUrl() {
        return "https://api.uwaterloo.ca/v2/events.json?key="+API_KEY+"&output=json";
    }
    
    @Override
    public String loadAPIData(String url) {
        return super.loadAPIData(url);
    }

    @Override
    public void loadBackgroundTask() {   
        super.loadBackgroundTask();
        try {
            listJSONResult = new JSONObject(loadAPIData(getAPIUrl())).getJSONArray("data");
            //Log.i("JSON: ", listJSONResult.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadUITask() {
     // Initialize today's date
        Calendar c = Calendar.getInstance();

        // set the calendar to start of today
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        
        c.add(Calendar.DAY_OF_YEAR, relDay);
        Date dayWeWant = c.getTime();
        
        // get tomorrow
        c.add(Calendar.DAY_OF_YEAR, relDay+1); // <--  
        Date dayAfterWeWant = c.getTime(); 
       
        Log.i("DAY BEFORE: ", dayWeWant.toString());
        Log.i("DAY AFTER: ", dayAfterWeWant.toString());
        try {
        
            for (int i = 0; i < listJSONResult.length(); i++) {
                JSONObject event = listJSONResult.getJSONObject(i);
                
                int eventId = event.getInt("id");
                String eventSite = event.getString("site");
                String eventTitle = event.getString("title");
                JSONArray eventTimes = event.getJSONArray("times");
                
                // Check if title is too long
                if (eventTitle.length() > 30) {
                    eventTitle = event.getString("title").substring(0, 30);
                }
                
                // Getting the first time for now in list
                JSONObject eventTimeObj = eventTimes.getJSONObject(0);
                
                Calendar startCal = Calendar.getInstance();
                Calendar endCal = Calendar.getInstance();
                
                startCal.setTime(df.parse(eventTimeObj.getString("start")));
                endCal.setTime(df.parse(eventTimeObj.getString("end")));
                
                if (new SimpleDateFormat("yyyy-MM-dd").format(startCal.getTime()).equals(new SimpleDateFormat("yyyy-MM-dd").format(dayWeWant.getTime()))) {
                    String eventStartTime = new SimpleDateFormat("hh:mm aa").format(startCal.getTime()).toLowerCase();
                    String eventEndTime = new SimpleDateFormat("hh:mm aa").format(endCal.getTime()).toLowerCase();
                    
                    String times = "";
                    if (startCal.equals(endCal)) {
                        times = eventStartTime;
                    } else {
                        times = eventStartTime+" - "+eventEndTime;
                    }
                    
                    // TODO: Lookup correct time, sort it and lookup people going to event
                    // For now, dummy values
                    cardList.add(new CardItemData("green",eventTitle, times, "None of your friends is attending"));
    
                    eventList.add(new EventDetail(eventId, eventSite));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.loadUITask();
    }
    
    
    @Override
    public void getSelectedCard(int position) {
        mCallback.onEventSelected(eventList.get(position).getId(), eventList.get(position).getSite());
    }
}