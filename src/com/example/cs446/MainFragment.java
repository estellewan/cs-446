package com.example.cs446;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

import com.example.cs446.interfaces.IFragment;

public class MainFragment extends IFragment {

    public MainFragment() {
        super();
    }
    // Registered API uwaterloo key
    protected String API_KEY = "801e2a06ff9686b4599da267caf86c7b";

    // TODO: remove later
    protected JSONArray getJSONFromAPIUrlAsJSONArray(String url) throws JSONException, Exception {
        return new JSONArray(getJSONFromAPIUrl(url));
    }
    
    protected JSONObject getJSONFromAPIUrlAsJSONObject(String url) throws JSONException, Exception {
        return new JSONObject(getJSONFromAPIUrl(url));
    }
    
    protected String getJSONFromAPIUrl(String url) throws Exception {
        
        HttpClient client = new DefaultHttpClient(); 
        HttpResponse responseGet = client.execute(new HttpGet(url));
        HttpEntity resEntityGet = responseGet.getEntity();
        
        if (resEntityGet != null) { 
            return EntityUtils.toString(resEntityGet);            
        }
        
        return null;
    }

    @Override
    public String loadAPIData(String url) {
        try {
            HttpClient client = new DefaultHttpClient(); 
            HttpResponse responseGet = client.execute(new HttpGet(url));
            HttpEntity resEntityGet = responseGet.getEntity();
            
            if (resEntityGet != null) { 
                return EntityUtils.toString(resEntityGet);            
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateView(int fragmentId) {
    }

    @Override
    public void loadUITask() {
    }

    @Override
    public void loadBackgroundTask() {
    }
    
    @Override
    public void loadAsyncTask() {
        //new YourAsyncTask().execute();
    }
    
    
}