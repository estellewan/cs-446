package com.example.cs446;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.cs446.EventFragment.OnEventSelectedListener;
import com.example.cs446.adapters.CardItemData;
import com.example.cs446.decorators.ListFragmentDecorator;
import com.example.cs446.interfaces.IFragment;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphMultiResult;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.GraphUser;
import com.tyczj.extendedcalendarview.CalendarProvider;


@SuppressLint("SimpleDateFormat")
public class MyScheduleFragment extends ListFragmentDecorator {
    
    // User id, need to grab from facebook login later
    protected long userId = 1;
    
    Calendar cal = null;
    
    List<Integer> courseIds = new ArrayList<Integer>();
    
    // to decide whether to update a session's info in the onActivityResult() method
    private static final int REAUTH_ACTIVITY_CODE = 100;
    
    List<GraphUser> friends = null;
    
    OnCourseSelectedListener mCallback;
    OnEventSelectedListener mECallback;
    OnUserIdListener mUserCallback;
    
    public MyScheduleFragment() {
        //listFragment = new MainFragment();
        super(null);
        listFragment = new MainFragment();
    }
    
    public MyScheduleFragment(IFragment listFragment) {
        super(listFragment);
    }
    
    // Container Activity must implement this interface
    public interface OnCourseSelectedListener {
        public void onCourseSelected(int id, List<GraphUser> friends);
    }
    
    public interface OnUserIdListener {
        public void setUserId(long id);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnCourseSelectedListener) activity;
            mECallback = (OnEventSelectedListener) activity;
            mUserCallback = (OnUserIdListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCourseSelectedListener");
        }
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);        
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, 
            ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_main, container, false);
        list = (ListView) view.findViewById(R.id.list_view);
        clearCardList();
        // Check for an open session
        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            // Get the user's data
            makeMeRequest(session);
        }
        return view;
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REAUTH_ACTIVITY_CODE) {
            uiHelper.onActivityResult(requestCode, resultCode, data);
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        uiHelper.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
        //clearCardList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
        //clearCardList();
    }
    
    @Override
    public void updateView(int fragmentId) {
        super.updateView(fragmentId);
    }

    // requests the user's data
    private void makeMeRequest(final Session session) {
        // Make an API call to get user data and define a 
        // new callback to handle the response.
        Request request = Request.newMeRequest(session, 
                new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                // If the response is successful
                if (session == Session.getActiveSession()) {
                    if (user != null) {
                        userId = Long.parseLong(user.getId());
                        mUserCallback.setUserId(userId);
                        addAuthenticatedToDb(user, session);
                        updateView(relDay);
                    }
                }
                if (response.getError() != null) {
                    // Handle errors, will do so later.
                }
            }

            
        });
        request.executeAsync();
    } 
    
    private void addAuthenticatedToDb(GraphUser user, Session session) {
        // TODO Auto-generated method stub
        SharedPreferences prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
        //if(prefs.contains("HAS_BEEN_RUN_FLAG")){
            //check the database for the user info
        //}else{            
            requestMyFacebookFriends(session);
            //do the facebook authentication
            prefs.edit().putInt("HAS_BEEN_RUN_FLAG", 1).commit();
        //}
    }
    
    private void requestMyFacebookFriends(Session session) {
        Request friendsRequest = createRequest(session);
        friendsRequest.setCallback(new Request.Callback()
        {

            @Override
            public void onCompleted(Response response)
            {
                friends = getResults(response);
            }
        });
        friendsRequest.executeAsync();
        
    }

    protected List<GraphUser> getResults(Response response) {
        GraphMultiResult multiResult = response
                .getGraphObjectAs(GraphMultiResult.class);
        GraphObjectList<GraphObject> data = multiResult.getData();
        return data.castToListOf(GraphUser.class);
    }

    private Request createRequest(Session session) {
        Request request = Request.newGraphPathRequest(session, "me/friends", null);
        
        Set<String> fields = new HashSet<String>();
        String[] requiredFields = new String[] { "id", "name", "picture",
                "gender" };
        fields.addAll(Arrays.asList(requiredFields));

        Bundle parameters = request.getParameters();
        parameters.putString("fields", TextUtils.join(",", fields));
        request.setParameters(parameters);

        return request;
    }

    // will respond to session changes and call the makeMeRequest() method if the session's open
    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            makeMeRequest(session);
        }
    }
    
    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    
    @Override
    public void addListToAdapter() {
        super.addListToAdapter();
    }

    @Override
    protected void clearCardList() {
        super.clearCardList();
        courseIds.clear();
    }
    
    @Override
    public String getAPIUrl() {
        String postDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        return "http://secret-oasis-8161.herokuapp.com/services/course/"+userId+"/"+postDate+"/1";
    }
    
    @Override
    public String loadAPIData(String url) {
        return super.loadAPIData(url);
    }
    
    @Override
    public void loadBackgroundTask() {   
        //super.loadBackgroundTask();
        try {
            cal = Calendar.getInstance();
            cal.add(Calendar.DATE, relDay);
            
            listJSONResult = new JSONArray(loadAPIData(getAPIUrl())).getJSONObject(0).getJSONArray("courses");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void loadUITask() {
        //clearCardList();
        //Log.i("UI TASK:","");
    	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdf2 = new SimpleDateFormat("h:mm aa");
        String startTime = "";
    	String endTime = "";
    	Date d;
        for (int i = 0; i < listJSONResult.length(); i++) {
            
            try {
                JSONObject classDetail = listJSONResult.getJSONObject(i);
                
                String courseName = classDetail.getString("subjectCode") + " " +classDetail.getString("subjectCatalog");
                
                JSONArray userList = classDetail.getJSONArray("userList");
                
                ArrayList<String>friendsGoing = new ArrayList<String>();
                
                // Gather friends enrolled in same course
                for (int k = 0; k < friends.size(); k++) {
                    for (int l = 0; l < userList.length(); l++) {
                        if (userList.getLong(l) == Long.parseLong(friends.get(k).getId())) {
                            friendsGoing.add(friends.get(k).getName());
                        }
                    }
                }
                
                String friendGoingStr;
                if (friendsGoing.size() == 0) 
                    friendGoingStr = "None of your friends is attending";
                else if (friendsGoing.size() == 1)
                    friendGoingStr = friendsGoing.get(0)+" is attending";
                else
                    friendGoingStr = TextUtils.join(", ", friendsGoing)+ " are attending";
                     
                startTime = classDetail.getString("startTime");
                endTime = classDetail.getString("endTime");
                
                d = sdf.parse(startTime);
                startTime = sdf2.format(d).toLowerCase();
                d = sdf.parse(endTime);
                endTime = sdf2.format(d).toLowerCase();
                
                cardList.add(new CardItemData("blue",
                        courseName, 
                        startTime+" - "+endTime, 
                        friendGoingStr.toString()));
                
                courseIds.add(classDetail.getInt("id"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        ContentResolver resolver = getActivity().getContentResolver();

        int startDayJulian = Time.getJulianDay(cal.getTimeInMillis(), TimeUnit.MILLISECONDS.toSeconds(cal.getTimeZone().getOffset(cal.getTimeInMillis())));
        Cursor cursor = resolver.query(CalendarProvider.CONTENT_URI, 
        		new String[]{
        		CalendarProvider.EVENT, 
        		CalendarProvider.START, 
        		CalendarProvider.END, 
        		CalendarProvider.ID,
        		CalendarProvider.LOCATION}, 
        		CalendarProvider.START_DAY+"=?", new String[] {String.valueOf(startDayJulian)}, null);
        
        while(cursor.moveToNext()) {
            String courseName = cursor.getString(0);
            cal.setTimeInMillis(cursor.getLong(1));
            startTime = sdf2.format(cal.getTime()).toLowerCase();
            cal.setTimeInMillis(cursor.getLong(2));
            endTime = sdf2.format(cal.getTime()).toLowerCase();
            String eventTime;
            if(startTime.equals(endTime))
            	eventTime = "All day event";
            else
            	eventTime = startTime + " - " + endTime;
            cardList.add(new CardItemData("green",
                    courseName, 
                    eventTime, 
                    "None of your friends is attending",
                    Integer.parseInt(cursor.getString(3)),
                    cursor.getString(4)
                    ));
        }
        cursor.close();      
        
        super.loadUITask();
    }
    
    // Launch detailed view of course activity
    protected void getSelectedCard(int position) {
        CardItemData cardSelected = cardList.get(position);
        
        if (cardSelected.getColor() == "green") {
            mECallback.onEventSelected(cardSelected.getId(), cardSelected.getSite());
        } else {
            mCallback.onCourseSelected(courseIds.get(position), friends);
        }
        
    }
    
    public void setActivityTitleWithDate() {
        super.setActivityTitleWithDate();
    }

    public void setRelativeDay(int relativeScheduleDay) {
        this.relDay = relativeScheduleDay;
    }    
}

