package com.example.cs446.adapters.inflaters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cs446.R;
import com.example.cs446.adapters.BaseInflaterAdapter;
import com.example.cs446.adapters.CardItemData;
import com.example.cs446.adapters.IAdapterViewInflater;
import com.example.cs446.models.Course;

/**
 * Created with IntelliJ IDEA.
 * User: Justin
 * Date: 10/6/13
 * Time: 12:47 AM
 */
public class CardCourseInflater implements IAdapterViewInflater<Course>
{
    @Override
    public View inflate(final BaseInflaterAdapter<Course> adapter, final int pos, View convertView, ViewGroup parent)
    {
        ViewHolder holder;

        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.list_course_details, parent, false);
            holder = new ViewHolder(convertView);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        final Course item = adapter.getTItem(pos);
        holder.updateDisplay(item);

        return convertView;
    }

    private class ViewHolder
    {
        private View m_rootView;
        private TextView m_text1;
        private TextView m_text2;
        private TextView m_text3;

        public ViewHolder(View rootView)
        {
            m_rootView = rootView;
            m_text1 = (TextView) m_rootView.findViewById(R.id.text1);
            m_text2 = (TextView) m_rootView.findViewById(R.id.text2);
            m_text3 = (TextView) m_rootView.findViewById(R.id.text3);
            rootView.setTag(this);
        }

        public void updateDisplay(Course item)
        {
            m_text1.setText(item.getSubjectCode()+" "+item.getSubjectCatalog());
            m_text2.setText(item.getTitle());
            m_text3.setText(item.getFormattedTimes());
        }
    }
}
