package com.example.cs446;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.cs446.models.Course;


public class AddCourseFragment extends MainFragment {
    
    private String[] subjectList;
    private String[] catalogList;
    private String[] sectionList;
    private int[] classNumberList;
    private Course course;
    
    private String[] sectionsTimes;
    
    public long userId = 1;
    private View view;

    private String getSubjectCodeAPIUrl() {
        return "http://api.uwaterloo.ca/v2/codes/subjects.json?key="+API_KEY+"&output=json";
    }
    
    private String getSubjectCatalogAPIUrl() {
        return "http://api.uwaterloo.ca/v2/courses/"+course.getSubjectCode()+".json?key="+API_KEY+"&output=json";
    }
    
    private String getSubjectClassesAPIUrl() {
        return "http://api.uwaterloo.ca/v2/courses/"+course.getSubjectCode()+"/"+course.getSubjectCatalog()+"/schedule.json?key="+API_KEY+"&output=json";
    }
    
    private String javaApiUrlDomain() {
        //return "localhost:5000";
        return "secret-oasis-8161.herokuapp.com";
    }
    
    /**
     * Is called when no data can be found for api url
     * @return A list of 1 item containing the string "None"
     */
    private String[] getEmptyListString() {
        String[] emptyStr = new String[1];
        emptyStr[0] = "None";
        return emptyStr;
    }
    
    /**
     * Creates an Http client to grad json object from url
     * @param url UWaterloo API url
     * @return An array of all items fetched from API under attribute "data"
     * @throws ClientProtocolException
     * @throws IOException
     * @throws JSONException
     */
    private JSONArray getJSONResultFromUrl(String url) throws ClientProtocolException, IOException, JSONException {
        Log.i("URL",url);
        
        HttpClient client = new DefaultHttpClient(); 
        HttpGet get = new HttpGet(url);
        HttpResponse responseGet = client.execute(get);
        HttpEntity resEntityGet = responseGet.getEntity();
        
        if (resEntityGet != null) { 
            String resStr = EntityUtils.toString(resEntityGet);
            JSONObject resJSON = new JSONObject(resStr);
            return resJSON.getJSONArray("data");
        }
        return null;
    }
    
    public class SubListAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... args) {
            try {
                subjectList = getSubjectList();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        
        protected void onPostExecute(Void result) {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_dropdown_item_1line, subjectList);
            AutoCompleteTextView subjectAutoTextView = (AutoCompleteTextView)
                    view.findViewById(R.id.subjectAuto);
            subjectAutoTextView.setAdapter(adapter);

            
            subjectAutoTextView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
                    String selection = (String) parent.getItemAtPosition(position);
                    loadSubjectIdSpinner(selection, view);
                }
            });
        }
    }
    
    /**
     * List of subject codes e.g. CS, PMATH, ECE, ..
     */
    private String[] getSubjectList() {
        try {
            String url = getSubjectCodeAPIUrl(); //The API service URL
            JSONArray dataList = getJSONResultFromUrl(url);

            if (dataList.length() == 0) return getEmptyListString();
            
            final String[] items = new String[dataList.length()];
            
            for(int i = 0; i < dataList.length(); i++){
                JSONObject dataItem = dataList.getJSONObject(i);
                items[i] = dataItem.getString("subject");
            }
            return items;      
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * List of subject catalog numbers e.g. 250, 446, 447, ..
     */
    @SuppressLint("NewApi")
    private String[] getSubjectCatalogNumber() {

        if (course.getSubjectCode().isEmpty()) return getEmptyListString();
        
        try {
            String url = getSubjectCatalogAPIUrl(); //The API service URL
            JSONArray dataList = getJSONResultFromUrl(url);
            
            if (dataList.length() == 0) return getEmptyListString();
            
            final String[] items = new String[dataList.length()];
            
            for(int i = 0; i < dataList.length(); i++){
                JSONObject dataItem = dataList.getJSONObject(i);
                items[i] = dataItem.getString("catalog_number");
            }
            return items;
        } catch(Exception e) {
            e.printStackTrace();
            return getEmptyListString();
        }
    }
    
    /**
     * List of sections for a course e.g. LEC 001, LAB 001, ..
     */
    @SuppressLint("NewApi")
    private String[] getCourseSectionList() {
        
        if (course.getSubjectCatalog().isEmpty()) return getEmptyListString();
        
        try {
            String url = getSubjectClassesAPIUrl(); //The API service URL
            JSONArray dataList = getJSONResultFromUrl(url);
            
            if (dataList.length() == 0) return getEmptyListString();
            
            final String[] items = new String[dataList.length()];
            classNumberList = new int[dataList.length()];
            
            sectionsTimes = new String[dataList.length()];
            
            for(int i = 0; i < dataList.length(); i++){
                JSONObject dataItem = dataList.getJSONObject(i);
                
                int class_number = dataItem.getInt("class_number");
                String section = dataItem.getString("section");
                
                items[i] = section;
                classNumberList[i] = class_number;
                
                // Get Times of classes
                JSONArray classes = dataItem.getJSONArray("classes");
                String classTimes = "";
                //Log.i("CLASSES", classes.toString());
                for(int k = 0; k < classes.length(); k++){
                    JSONObject dateObj = classes.getJSONObject(k).getJSONObject("date");
                    
                    if (dateObj.getBoolean("is_tba")) {
                        classTimes += "TBA";
                        continue;
                    }
                    
                    classTimes += dateObj.getString("weekdays")+" ";
                    classTimes += dateObj.getString("start_time")+"-"+dateObj.getString("end_time");
                    classTimes += " ";
                }
                
                
                sectionsTimes[i] = classTimes;
                // TODO: Add building and room for section
            }
            return items;  
        } catch(Exception e) {
            e.printStackTrace();
            return getEmptyListString();
        }
    }
    
    TextView text4;
    
    public class CourseSecAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... args) {
            sectionList = getCourseSectionList();
            return null;
        }
        
        protected void onPostExecute(Void result) {
         // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                     android.R.layout.simple_spinner_item, sectionList);
            
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Spinner spinner = (Spinner) view.findViewById(R.id.subject_sections_spinner);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);
            
            text4 = (TextView) view.findViewById(R.id.textView4);
            
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {                
                    
                    if (sectionsTimes == null || sectionsTimes[position] == null) {
                        text4.setText("Information not available");
                    } else {
                        // Show times to user
                        text4.setText(sectionsTimes[position]);
                        // Set params ready for sending to db
                        course.setId(classNumberList[position]);
                        course.setSection(sectionList[position]);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            }); 
        }
    }
    
    private void loadCourseSectionSpinner(int index, View view) {
        Log.i("SPINNER", "Loading 3nd spinner for "+catalogList[index]);
        
        
        course.setSubjectCatalog(catalogList[index]);
        //sectionList = getCourseSectionList();
        new CourseSecAsyncTask().execute();
        
    }

    public class SubIdAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... args) {
            catalogList = getSubjectCatalogNumber();
            return null;
        }
        
        protected void onPostExecute(Void result) {
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                     android.R.layout.simple_spinner_item, catalogList);
            
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            
            Spinner subIdSpinner = (Spinner) view.findViewById(R.id.subject_ids_spinner);
            // Apply the adapter to the spinner
            subIdSpinner.setAdapter(adapter);
            
            subIdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {                
                    loadCourseSectionSpinner(position, view);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }
    
    private void loadSubjectIdSpinner(String subject, View view) {
        Log.i("SPINNER", "Loading 2nd spinner for "+subject);
        
        
        course.setSubjectCode(subject);
        //catalogList = getSubjectCatalogNumber();
        new SubIdAsyncTask().execute();
         
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, 
            ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.activity_add_course, container, false);
        
        try {
        // Load subject list
        //subjectList = getSubjectList();
        new  SubListAsyncTask().execute();
        
        // Create course object
        course = new Course();

        // Create the text view
        //Spinner spinner = (Spinner) findViewById(R.id.subjects_spinner);
        
        // Create an ArrayAdapter using the string array and a default spinner layout

        } catch(Exception e) {
            e.printStackTrace();
        }
        
        return view;
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public class YourAsyncTask extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... args) {
            try {
                Map <String, String> params = new HashMap<String, String>();
                
                params.put("courseId", Integer.toString(course.getId()));
                params.put("userId", Long.toString(userId));
                
                DefaultHttpClient httpclient = new DefaultHttpClient();
                
                HttpPost httpost = new HttpPost("http://"+javaApiUrlDomain()+"/services/course/add-course-user");
                
                Iterator iter = params.entrySet().iterator();
        
                JSONObject holder = new JSONObject();
        
                // Add BODY parameters
                while(iter.hasNext()) {
                    Map.Entry pairs = (Map.Entry)iter.next();
                    
                    String key = (String)pairs.getKey();
                    String data = (String)pairs.getValue();
                    
                    holder.put(key, data);
                }
        
                StringEntity se = new StringEntity(holder.toString());
                httpost.setEntity(se);
                httpost.setHeader("Accept", "application/json");
                httpost.setHeader("Content-type", "application/json");
        
                ResponseHandler responseHandler = new BasicResponseHandler();
                Object response = httpclient.execute(httpost, responseHandler);
                
                return "";
            } catch (Exception e) {
                e.printStackTrace();
                //launchNotOkDialog(e.getMessage());
                return e.getMessage();
            }
            
        }
        
        protected void onPostExecute(String result) {
            if (result.length() == 0) {
                launchOkDialog();
            } else {
                launchNotOkDialog(result);
            }
        }
    }
    
    /**
     * Saves course into database
     * @param view
     * @throws JSONException 
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    public void addCourse(View view) {
        try {
            new YourAsyncTask().execute();
            //launchOkDialog();
            
        }  catch (Exception e) {
            e.printStackTrace();
            //launchNotOkDialog(e.getMessage());
        }
    }

    private void launchNotOkDialog(String msg) {
        AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity())
        .setMessage(msg)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // TODO
            }
         })
        .setIcon(android.R.drawable.ic_dialog_alert)
         .show();
    }

    private void launchOkDialog() {
        AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity())
        .setMessage("Your course has been added!")
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // TODO
            }
         })
         .show();
    }
}