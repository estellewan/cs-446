package com.example.cs446;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.cs446.CourseDetailFragment.OnCourseDetailLoadListener;
import com.example.cs446.adapters.BaseInflaterAdapter;
import com.example.cs446.adapters.inflaters.CardEventInflater;
import com.example.cs446.models.EventDetail;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tyczj.extendedcalendarview.CalendarProvider;
import com.tyczj.extendedcalendarview.Event;


@SuppressLint("SimpleDateFormat")
public class EventDetailFragment extends MainFragment {
    
    ListView list = null;
    JSONObject eventJson = null;
    EventDetail event = null;
    LatLng eventLoc = null;
    double lat;
    double lon;
    String locName = "";
    String website = "https://uwaterloo.ca/events/";
    
    public long userId = 1;    
    
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    SimpleDateFormat sdf = new SimpleDateFormat("EEE dd MMM");
    
    BaseInflaterAdapter<EventDetail> adapter = new BaseInflaterAdapter<EventDetail>(new CardEventInflater());
    
    OnEventDetailLoadListener mCallback;
    
    public interface OnEventDetailLoadListener {
        public void onEventDetailLoadSelected(String title);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnEventDetailLoadListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCourseDetailLoadListener");
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, 
            ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_event_details, container, false);
        
        list = (ListView) view.findViewById(R.id.list_view);
        return view;
    }
    
    public void updateFragmentView(int id, String site) {

        adapter.clear(false);
        event = new EventDetail(id, site);
        
        new YourAsyncTask().execute();

    }
    
    private class YourAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... args) {
        	//String locStreet;
        	//String 
            try {
                
                eventJson = getJSONFromAPIUrlAsJSONObject("https://api.uwaterloo.ca/v2/events/"+event.getSite()+"/"+event.getId()+".json?key="+API_KEY+"&output=json").getJSONObject("data");
                
                // TODO: start and end time might be null
                event.setTitle(eventJson.getString("title"));
                event.setDescription(eventJson.getString("description"));
                event.setStart(eventJson.getJSONArray("times").getJSONObject(0).getString("start"));
                event.setEnd(eventJson.getJSONArray("times").getJSONObject(0).getString("end"));
                
                locName = eventJson.getJSONObject("location").getString("name");
                website = eventJson.getString("link");
                
                lat = eventJson.getJSONObject("location").getDouble("latitude");
                lon = eventJson.getJSONObject("location").getDouble("longitude");
                eventLoc = new LatLng(lat, lon);
                
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        
        protected void onPostExecute(Void res) {
            // Add event details such as category, title, times, ...
            adapter.addItem(event, false);
            list.setAdapter(adapter);
            //getActivity().setTitle(event.getTitle());
            mCallback.onEventDetailLoadSelected(event.getTitle());
            findEventLocation(getView());
        }
    }
    
    private class YourSecondAsyncTask extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... args) {
            try {
                addEventToMySQLdb();
                return "";
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return e.getMessage();
            }
        }
        protected void onPostExecute(String res) {
            if (res.length() == 0) {
                //launchOkDialog();
            } else {
                launchNotOkDialog(res);
            }
        }
    }
    
    public void addEvent(View view) {
        
        
    	if(deleteEvent(view) == 0) {

    	        new YourSecondAsyncTask().execute();
    	        try {
        ContentValues values = new ContentValues();
        values.put(CalendarProvider.COLOR, Event.COLOR_GREEN);
        values.put(CalendarProvider.DESCRIPTION, event.getDescription());
        values.put(CalendarProvider.LOCATION, event.getSite());
        values.put(CalendarProvider.EVENT, event.getTitle());
        values.put(CalendarProvider.ID, Integer.toString(event.getId()));

        Calendar calStart = Calendar.getInstance();
        Calendar calEnd = Calendar.getInstance();
        TimeZone tz = TimeZone.getDefault();

        String eventStart = event.getStart();
        String eventEnd = event.getEnd();
        
        
            calStart.setTime(df.parse(eventStart));
            values.put(CalendarProvider.START, calStart.getTimeInMillis());
            int startDayJulian = Time.getJulianDay(calStart.getTimeInMillis(), TimeUnit.MILLISECONDS.toSeconds(calStart.getTimeZone().getOffset(calStart.getTimeInMillis())));
            values.put(CalendarProvider.START_DAY, startDayJulian);
            
            calEnd.setTime(df.parse(eventEnd));
            int endDayJulian = Time.getJulianDay(calEnd.getTimeInMillis(), TimeUnit.MILLISECONDS.toSeconds(calEnd.getTimeZone().getOffset(calEnd.getTimeInMillis())));
            values.put(CalendarProvider.END, calEnd.getTimeInMillis());
            values.put(CalendarProvider.END_DAY, endDayJulian);

            Uri uri = getActivity().getContentResolver().insert(CalendarProvider.CONTENT_URI, values);
            launchOkDialog();
            //Toast.makeText(getBaseContext(),"Event " + event.getTitle() + " added!", Toast.LENGTH_LONG).show();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            launchNotOkDialog(e.getMessage());
        }

    	} else {
    		launchDeletedDialog();
    	}
    }
    
    public int deleteEvent(View view) {
    	
    	ContentResolver resolver = getActivity().getContentResolver();
    	
    	int deletions = resolver.delete(CalendarProvider.CONTENT_URI, 
        		CalendarProvider.ID+"=?", 
        		new String[] {String.valueOf(event.getId())}); 	
    	return deletions;
    }
    
    

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void addEventToMySQLdb() throws Exception {
        try {
            Map <String, String> params = new HashMap<String, String>();
           
            params.put("eventId", Integer.toString(event.getId()));
            params.put("eventSite", event.getSite());
            params.put("userId", Long.toString(userId));
            
            DefaultHttpClient httpclient = new DefaultHttpClient();
            
            HttpPost httpost = new HttpPost("http://secret-oasis-8161.herokuapp.com/services/event/add-event-user");
            
            Iterator iter = params.entrySet().iterator();
    
            JSONObject holder = new JSONObject();
    
            // Add BODY parameters
            while(iter.hasNext()) {
                Map.Entry pairs = (Map.Entry)iter.next();
                
                String key = (String)pairs.getKey();
                String data = (String)pairs.getValue();
                
                holder.put(key, data);
            }
    
            StringEntity se = new StringEntity(holder.toString());
            httpost.setEntity(se);
            httpost.setHeader("Accept", "application/json");
            httpost.setHeader("Content-type", "application/json");
    
            ResponseHandler responseHandler = new BasicResponseHandler();
            Object response = httpclient.execute(httpost, responseHandler);
            
            //launchOkDialog();
            
        }  catch (Exception e) {
            e.printStackTrace();
            //launchNotOkDialog(e.getMessage());
            throw e;
        }
        
    }
        
    public void findEventLocation(View view) {
	
        FragmentManager fragManager = getFragmentManager();
        GoogleMap map = ((SupportMapFragment) fragManager.findFragmentById(R.id.eventMap)).getMap();
        map.clear();
        map.setMyLocationEnabled(true);
        
        if(eventLoc != null) {
	        map.addMarker(new MarkerOptions().position(eventLoc).title(locName));
	        map.animateCamera(CameraUpdateFactory.newLatLngZoom(eventLoc, 15));
        }
        else {
        	//No location specified
        	//Online course?
        	LatLng loo = new LatLng(43.472281, -80.544858);
        	map.addMarker(new MarkerOptions().position(loo).title("Waterloo University"));
        }
    	
    }
    
    public void openEventSite(View view) {
    	
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
        startActivity(browserIntent);
    	
    }

    private void launchNotOkDialog(String msg) {
        AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity())
        .setMessage(msg)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // TODO
            }
         })
        .setIcon(android.R.drawable.ic_dialog_alert)
         .show();
    }

    private void launchOkDialog() {
        AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity())
        .setMessage("Your event has been added!")
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // TODO
            }
         })
         .show();
    }
    
    private void launchDeletedDialog() {
        AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity())
        .setMessage("Your event has been deleted!")
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // TODO
            }
         })
         .show();
    }
}