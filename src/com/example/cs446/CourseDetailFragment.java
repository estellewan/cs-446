package com.example.cs446;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.cs446.adapters.BaseInflaterAdapter;
import com.example.cs446.adapters.inflaters.CardCourseInflater;
import com.example.cs446.models.Course;
import com.facebook.model.GraphUser;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class CourseDetailFragment extends MainFragment {
    
    private ListView list = null;
    private Course course = null;
    JSONObject courseJson = null;
    JSONObject buildingJson = null;
    LatLng buildingLoc = null;
    String courseRoom = "";
    String courseBuilding = "";
    double lat;
    double lon;
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    SimpleDateFormat sdf2 = new SimpleDateFormat("h:mm aa");
    BaseInflaterAdapter<Course> adapter = new BaseInflaterAdapter<Course>(new CardCourseInflater());
    OnCourseDetailLoadListener mCallback;
    List<GraphUser> friends;
    long userId;
    JSONArray userList = null;
    
    public interface OnCourseDetailLoadListener {
        public void onCourseDetailLoadSelected(String title);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnCourseDetailLoadListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCourseDetailLoadListener");
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, 
            ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_course_details, container, false);

        //list = (ListView)view.findViewById(R.id.list_view);
        
        dialog = new ProgressDialog(getActivity());
        
        return view;
    }

    public void updatedFragmentView(int id, long mUserId, List<GraphUser> mfriends) {
        userId = mUserId;
        friends = mfriends;
        adapter.clear(false);
        friendsGoingUrls.clear();
        friendsGoing.clear();
        course = new Course();
        course.setId(id);
        numUser = 0;
        
        new YourAsyncTask().execute();
        new YourSecondAsyncTask().execute();

    }
    
    private class FriendImageAsync extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... args) {
            for (int k = 0; k < friendsGoing.size(); k++) {
                URL url;
                Bitmap bitmap = null;
                try {
                    url = new URL("http://graph.facebook.com/"+friendsGoing.get(k).getId()+"/picture?type=square&height=200&width=200");
                    //try this url = "http://0.tqn.com/d/webclipart/1/0/5/l/4/floral-icon-5.jpg"
                    HttpGet httpRequest = null;

                    httpRequest = new HttpGet(url.toURI());

                    HttpClient httpclient = new DefaultHttpClient();
                    HttpResponse response = (HttpResponse) httpclient
                            .execute(httpRequest);

                    HttpEntity entity = response.getEntity();
                    BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
                    InputStream input = b_entity.getContent();

                    bitmap = BitmapFactory.decodeStream(input);

                    //img.setImageBitmap(bitmap);
                    //url = new URL((String) "http://graph.facebook.com/"+friends.get(k).getId()+"/picture?type=small");
                    //bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    friendsGoingUrls.add(bitmap);
                    //url.openConnection().getInputStream().reset();
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
            }
            return null;
        }
        
        protected void onPostExecute(Void res) {
            Log.i("friends: ",friendsGoing.toString() );
            Log.i("friends1: ",userList.toString() );
            List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
            numUser = 0;
            for(int i=0;i<friendsGoing.size();i++){
                HashMap<String, String> hm = new HashMap<String,String>();
                hm.put("txt", friendsGoing.get(i).getName());
                hm.put("flag", "http://graph.facebook.com/"+friendsGoing.get(i).getId()+"/picture?type=large" );
                aList.add(hm);
            }
         // Keys used in Hashmap
            String[] from = { "flag","txt" };

            // Ids of views in listview_layout
            int[] to = { R.id.flag,R.id.txt};
            
         // Instantiating an adapter to store each items
            // R.layout.listview_layout defines the layout of each item
            SimpleAdapter sadapter = new SimpleAdapter(getActivity().getBaseContext(), aList,
                    R.layout.list_friends, from, to);
            
            // Getting a reference to listview of main.xml layout file
            ListView listView = ( ListView ) getActivity().findViewById(R.id.list_view_friends);

            sadapter.setViewBinder(new SimpleAdapter.ViewBinder() {

                @Override
                public boolean setViewValue(View view, Object data, String textRepresentation) {
                    if(view.getId() == R.id.flag) {
                        ImageView imageView = (ImageView) view;
                        if (numUser < friendsGoingUrls.size())
                            imageView.setImageBitmap(friendsGoingUrls.get(numUser));
                        numUser++;
                        return true;
                    }
                    return false;
                }
            });
            // Setting the adapter to the listView
            listView.setAdapter(sadapter);
        }
    }
    
    int numUser = 0;
    ProgressDialog dialog = null;
    //ArrayList<Bitmap>urls = new ArrayList<Bitmap>();
    ArrayList<Bitmap>friendsGoingUrls = new ArrayList<Bitmap>();
    ArrayList<GraphUser>friendsGoing = new ArrayList<GraphUser>();
    private class YourAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            dialog.setMessage("Loading course");
            dialog.show();
        }

        @SuppressLint("NewApi")
        protected Void doInBackground(Void... args) {
            
            //dialog.setMessage("Please wait");
            //dialog.show();
        	String startTime = "";
        	String endTime = "";
        	Date d;
        	
            try {
            	
                courseJson = getJSONFromAPIUrlAsJSONObject("https://api.uwaterloo.ca/v2/courses/"+course.getId()+"/schedule.json?key="+API_KEY+"&output=json").getJSONArray("data").getJSONObject(0);
                //Log.i("C: ", "https://api.uwaterloo.ca/v2/courses/"+course.getId()+"/schedule.json?key="+API_KEY+"&output=json");
                course.setSubjectCode(courseJson.getString("subject"));
                course.setSubjectCatalog(courseJson.getString("catalog_number"));
                course.setTitle(courseJson.getString("title"));
                //course.setDescription(courseJson.getString("description"));
                
                startTime = courseJson.getJSONArray("classes").getJSONObject(0).getJSONObject("date").getString("start_time");
                endTime = courseJson.getJSONArray("classes").getJSONObject(0).getJSONObject("date").getString("end_time");
                
                d = sdf.parse(startTime);
                startTime = sdf2.format(d).toLowerCase();
                d = sdf.parse(endTime);
                endTime = sdf2.format(d).toLowerCase();
                
                course.setStart(startTime);
                course.setEnd(endTime);
                
                courseBuilding = courseJson.getJSONArray("classes").getJSONObject(0).getJSONObject("location").getString("building");
        		courseRoom = courseJson.getJSONArray("classes").getJSONObject(0).getJSONObject("location").getString("room");

        		// Get users enrolled in class
        		String resAPI = getJSONFromAPIUrl("http://secret-oasis-8161.herokuapp.com/services/course/list-user/"+course.getId());

        		userList = new JSONArray(resAPI);
        		//userList = new JSONArray();
        		//userList.put(10152635154254101L);
        		//userList.put(10152519148432457L);
        		
            } catch (Exception e) {
                e.printStackTrace();
            }
                       
            return null;
        }
        
        protected void onPostExecute(Void res) {
            // Add event details such as category, title, times, ...
            TextView m_text1;
            TextView m_text2;
            TextView m_text3;
            //adapter.addItem(course, false);
            m_text1 = (TextView) getView().findViewById(R.id.text1);
            m_text2 = (TextView) getView().findViewById(R.id.text2);
            m_text3 = (TextView) getView().findViewById(R.id.text3);
            
            m_text1.setText(course.getTitle());
            m_text2.setText(courseBuilding +" "+courseRoom);
            m_text3.setText(course.getFormattedTimes());

            mCallback.onCourseDetailLoadSelected(course.getFullCourseTitle());
            
            numUser = 0;
            
            
            // Gather friends enrolled in same course
            if (userList != null) {
            for (int k = 0; k < friends.size(); k++) {
                for (int l = 0; l < userList.length(); l++) {
                    try {
                        if (userList.getLong(l) == Long.parseLong(friends.get(k).getId())) {
                            friendsGoing.add(friends.get(k));
                            //friendsGoingUrls.add(urls.get(k));
                        }
                    } catch (NumberFormatException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            
            new FriendImageAsync().execute();
            } else {
                // if no friends are enrolled
            }
            
            
         
        }
        
        
    }

    
    private class YourSecondAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... args) {
        	
        	try {
        		
	        	buildingJson = getJSONFromAPIUrlAsJSONObject("https://api.uwaterloo.ca/v2/buildings/"+courseBuilding+".json?key="+API_KEY+"&output=json").getJSONObject("data");
	            lat = buildingJson.getDouble("latitude");
	            lon = buildingJson.getDouble("longitude");
	            buildingLoc = new LatLng(lat, lon);
        	
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        	return null;
        }
        
        protected void onPostExecute(Void res) {
            findCourseLocation(getView());
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            //getView().findViewById(R.id.ProgressBar).setVisibility(getView().INVISIBLE);
        }
        
    }

    
    public void findCourseLocation(View view) {
	
        FragmentManager fragManager = getFragmentManager();
        GoogleMap map = ((SupportMapFragment) fragManager.findFragmentById(R.id.courseMap)).getMap();
        map.clear();
        map.setMyLocationEnabled(true);
        
        if(buildingLoc != null) {
	        map.addMarker(new MarkerOptions().position(buildingLoc).title(courseBuilding + " " + courseRoom));
	        map.animateCamera(CameraUpdateFactory.newLatLngZoom(buildingLoc, 15));
        }
        else {
        	//No location specified
        	//Online course?
        }
    	
    }
}