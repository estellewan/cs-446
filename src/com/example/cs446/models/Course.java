package com.example.cs446.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;

@SuppressLint("SimpleDateFormat")
public class Course {

    private int id;
    private String subject_code;
    private String subject_catalog;
    private String title;
    private String description;
    private String section;
    private String weekdays;
    private String start;
    private String end;
    
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public Course() {}
    public Course(int id, String subject_code, String subject_catalog, String section) {
     this.id = id;
        this.subject_code = subject_code;
        this.subject_catalog = subject_catalog;
        this.section = section;
    }
    
    public int getId() {
        return id;
    }
    
    public String getSubjectCode() {
        return subject_code;
    }

    public String getSubjectCatalog() {
        return subject_catalog;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getSection() {
        return section;
    }
    
    public String getWeekdays() {
        return weekdays;
    }
    
    public String getStart() {
        return start;
    }
    
    public String getEnd() {
        return end;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setSubjectCode(String subjectCode) {
        this.subject_code = subjectCode;
    }

    public void setSubjectCatalog(String subjectCatalog) {
        this.subject_catalog = subjectCatalog;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setSection(String section) {
        this.section = section;
    }
    
    public void setWeekdays(String weekdays) {
        this.weekdays = weekdays;
    }
    
    public void setStart(String start) {
        this.start = start;
    }
    
    public void setEnd(String end) {
        this.end = end;
    }
    
    public String getFormattedTimes() {
        return start+" - "+end;
    }
    
    public String getFullCourseTitle() {
        return this.subject_code+ " "+this.getSubjectCatalog();
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}