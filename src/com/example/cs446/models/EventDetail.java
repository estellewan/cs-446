package com.example.cs446.models;

import android.annotation.SuppressLint;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * This class model reflect the attributes from UWaterloo API
 * See https://github.com/uWaterloo/api-documentation/blob/master/v2/events/events_site_id.md
 * 
 * @author Estelle
 *
 */
@SuppressLint("SimpleDateFormat")
public class EventDetail {

    private int m_id;
    private String m_site;
    private String m_title;
    private String m_description;
    private String m_start;   // ISO 8601 formatted start date
    private String m_end;     // ISO 8601 formatted end date
    
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    
    public EventDetail(int id, String site) {
        m_id = id;
        m_site = site;
    }
    
    public int getId() {
        return m_id;
    }
    
    public String getSite() {
        return m_site;
    }
    
    public String getTitle() {
        return m_title;
    }
    
    public String getDescription() {
        return m_description;
    }
    
    public String getFormattedTimes() {
        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();
        String times = "";
        
        if (m_start != null || m_end != null) {
            try {
                startCal.setTime(df.parse(m_start));
                endCal.setTime(df.parse(m_end));
                
                
                String eventStartTime = new SimpleDateFormat("h:mm aa").format(startCal.getTime()).toLowerCase();
                String eventEndTime = new SimpleDateFormat("h:mm aa").format(endCal.getTime()).toLowerCase();
                
                
                if (startCal.equals(endCal)) {
                    times = eventStartTime;
                } 
                else if (eventStartTime.equals(eventEndTime)) {
                	times = "All day event";
                }
                else {
                    times = eventStartTime+" - "+eventEndTime;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            times = "Not available";
        }
        return times;
    }
    
    public void setTitle(String title) {
        m_title = title;
    }
    
    public void setDescription(String descrp) {
        m_description = descrp;
    }
    
    public void setStart(String start) {
        m_start = start;
    }
    
    public void setEnd(String end) {
        m_end = end;
    }
        
    public String getStart() {
        return m_start;
    }
    
    public String getEnd() {
        return m_end;
    }
}