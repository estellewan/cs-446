package com.example.cs446.interfaces;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;

public abstract class IFragment extends Fragment {
    
    
    public abstract void updateView(int fragmentId);
    public abstract String loadAPIData(String url);
    public abstract void loadUITask();
    public abstract void loadBackgroundTask();
    public abstract void loadAsyncTask();
}