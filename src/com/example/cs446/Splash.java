package com.example.cs446;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.cs446.adapters.DrawerAdapter;
import com.example.cs446.models.DrawerItem;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;

public class Splash extends FragmentActivity 
    implements CalendarFragment.OnDaySelectedListener, 
        MyScheduleFragment.OnCourseSelectedListener,
        EventFragment.OnEventSelectedListener,
        MyScheduleFragment.OnUserIdListener,
        CourseDetailFragment.OnCourseDetailLoadListener,
        EventDetailFragment.OnEventDetailLoadListener
{
    private static final int SPLASH = 0;
    private static final int MYSCHEDULE = 1;
    private static final int CALENDAR = 2;
    private static final int EVENT = 3;
    private static final int ADDCOURSE = 4;
    protected static final int SIGNOUT = 5;
    private static final int COURSEDETAIL = 6;
    private static final int EVENTDETAIL = 7;
    
    protected static final int PREVIOUS = 8;
    protected static final int NEXT = 9;
    
    private static final int FRAGMENT_COUNT = SIGNOUT +1;
    
    private int relativeScheduleDay = 0;

    private MenuItem[] menuItems = new MenuItem[FRAGMENT_COUNT];
    
    private Fragment splashFragment = null;
    private MyScheduleFragment myScheduleFragment = null;
    private CalendarFragment calendarFragment = null;
    private EventFragment eventFragment = null;
    private AddCourseFragment addCourseFragment = null;
    private Fragment signoutFragment = null;
    private CourseDetailFragment courseDetailFragment = null;
    private EventDetailFragment eventDetailFragment = null;

    // This flag is used to enable session state change checks
    private boolean isResumed = false;
    
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    List<DrawerItem> dataList;
    DrawerAdapter adapter;
    
    int currentFrag = 0;
    
    public long userId = 1;
    
    // Set global user id and propagate to fragments
    public void setUserId(long id) {
        userId = id;
        addCourseFragment.userId = id;
        eventDetailFragment.userId = id;
    }

    // ===== Showing a given fragment and hiding all other fragments ======
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void showFragment(int fragmentIndex, boolean addToBackStack, boolean update) {
        
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        
        hideAllFragments();
        
        
        
        switch(fragmentIndex) {
        case SPLASH:
            transaction.show(splashFragment);
            getActionBar().setTitle("Scheduloo");
            break;
        case MYSCHEDULE:
            //myScheduleFragment.setRelativeDay(relativeScheduleDay);
            transaction.show(myScheduleFragment);
            if (update) myScheduleFragment.updateView(relativeScheduleDay);
            getActionBar().setTitle(myScheduleFragment.getTitleWithDate());
            break;
        case CALENDAR:
            transaction.show(calendarFragment);
            getActionBar().setTitle(R.string.title_activity_calendar);
            break;
        case EVENT:
            //eventFragment.setRelativeDay(relativeScheduleDay);
            transaction.show(eventFragment);
            if (update) eventFragment.updateView(relativeScheduleDay);
            getActionBar().setTitle(eventFragment.getTitleWithDate());
            
            break;
        case ADDCOURSE:
            transaction.show(addCourseFragment);
            getActionBar().setTitle(R.string.title_activity_add_course);
            break;
        case SIGNOUT:
            transaction.show(signoutFragment);
            getActionBar().setTitle(R.string.title_activity_log_out);
            break;
        case COURSEDETAIL:
            transaction.show(courseDetailFragment);
            break;
        case EVENTDETAIL:
            transaction.show(eventDetailFragment);
            break;
        }
        
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        
        currentFrag = fragmentIndex;
        invalidateOptionsMenu();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showFragmentWithParams() {
        if (myScheduleFragment.isVisible()) {
            myScheduleFragment.updateView(relativeScheduleDay);
            getActionBar().setTitle(myScheduleFragment.getTitleWithDate());
        } else if (eventFragment.isVisible()) {
            eventFragment.updateView(relativeScheduleDay);
            getActionBar().setTitle(eventFragment.getTitleWithDate());
        }
        
        if (relativeScheduleDay == 0)
            thisMenu.removeItem(R.id.action_previous);
        else if (thisMenu.findItem(R.id.action_previous) == null) 
            addPreviousItem();
    }
    
    // =======================================================================
    
    /**
     * It will be called due to session state changes. The method shows the 
     * relevant fragment based on the person's authenticated state. 
     * Only handle UI changes when the activity is visible by making use of the isResumed flag. 
     * The fragment back stack is first cleared before the showFragment() 
     * method is called with the appropriate fragment info
     * 
     * @param session
     * @param state
     * @param exception
     */
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        // Only make changes if the activity is visible
        if (isResumed) {
            FragmentManager manager = getSupportFragmentManager();
            // Get the number of entries in the back stack
            int backStackSize = manager.getBackStackEntryCount();
            // Clear the back stack
            for (int i = 0; i < backStackSize; i++) {
                manager.popBackStack();
            }
            if (state.isOpened()) {
                
                if (!session.isPermissionGranted("user_friends")) {
                Session.NewPermissionsRequest newPermissionsRequest = new Session
                        .NewPermissionsRequest(this, Arrays.asList("user_friends"));
                      session.requestNewReadPermissions(newPermissionsRequest);
                }
                // If the session state is open:
                // Show the authenticated fragment
                showFragment(MYSCHEDULE, false, false);
            } else if (state.isClosed()) {
                // If the session state is closed:
                // Show the login fragment
                showFragment(SPLASH, false, false);
            }
        }
    }
    
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        Session session = Session.getActiveSession();

        if (session != null && session.isOpened()) {
            // if the session is already open,
            // try to show the selection fragment
            showFragment(MYSCHEDULE, false, false);
        } else {
            // otherwise present the splash screen
            // and ask the person to login.
            showFragment(SPLASH, false, false);
        }
    }
    
    // to track the session and trigger a session state change listener
    private UiLifecycleHelper uiHelper;
    // The Session.StatusCallback implementation overrides the call() method 
    // and invokes the private onSessionStateChange() method that you previously defined
    private Session.StatusCallback callback = 
        new Session.StatusCallback() {
        @Override
        public void call(Session session, 
                SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    
    private void hideAllFragments() {
        
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        //if (splashFragment.isVisible())
            transaction.hide(splashFragment);
        //if (myScheduleFragment.isVisible())
            transaction.hide(myScheduleFragment).addToBackStack(null);
        //if (calendarFragment.isVisible()) 
            transaction.hide(calendarFragment).addToBackStack(null);
        //if (eventFragment.isVisible())
            transaction.hide(eventFragment).addToBackStack(null);
        //if (addCourseFragment.isVisible())
            transaction.hide(addCourseFragment).addToBackStack(null);
        //if (signoutFragment.isVisible())
            transaction.hide(signoutFragment).addToBackStack(null);
        //if (courseDetailFragment.isVisible())
            transaction.hide(courseDetailFragment).addToBackStack(null);
        //if (eventDetailFragment.isVisible())
            transaction.hide(eventDetailFragment).addToBackStack(null);
        //transaction.addToBackStack(null);
        transaction.commit(); 
    }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onBackPressed()
    {
    	getSupportFragmentManager().popBackStack();
    	super.onBackPressed();
                
        if (myScheduleFragment.isVisible()) {
            currentFrag = MYSCHEDULE;
            getActionBar().setTitle(myScheduleFragment.getTitleWithDate());
        } else if (calendarFragment.isVisible()) {
            currentFrag = CALENDAR;
            getActionBar().setTitle(R.string.title_activity_calendar);
        } else if (eventFragment.isVisible()) {
            currentFrag = EVENT;
            getActionBar().setTitle(eventFragment.getTitleWithDate());
        } else if (addCourseFragment.isVisible()) {
            currentFrag = ADDCOURSE;
            getActionBar().setTitle(R.string.title_activity_add_course);
        } else if (signoutFragment.isVisible()) {
            getActionBar().setTitle(R.string.title_activity_log_out);
            currentFrag = SIGNOUT;
        }        
        invalidateOptionsMenu();
    }
    
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            showFragment(position+1, true, true);
        }
    }

    //private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    
    /** Called when the activity is first created. */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(icicle);
        
        setContentView(R.layout.main);
        
        dataList = new ArrayList<DrawerItem>();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        dataList.add(new DrawerItem("MySchedule", R.drawable.ic_action_view_as_list));
        dataList.add(new DrawerItem("Calendar", R.drawable.ic_action_go_to_today));
        dataList.add(new DrawerItem("Events", R.drawable.ic_action_event));
        dataList.add(new DrawerItem("Add a Course", R.drawable.ic_action_new_event));
        dataList.add(new DrawerItem("Sign out", R.drawable.ic_action_settings));
        
        adapter = new DrawerAdapter(this, R.layout.drawer_list_item,
                dataList);

        mDrawerList.setAdapter(adapter);
        // Set the adapter for the list view
        //mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                //R.layout.drawer_list_item, R.id.drawer_itemName, mPlanetTitles));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        FragmentManager fm = getSupportFragmentManager();
        splashFragment = fm.findFragmentById(R.id.splashFragment);
        myScheduleFragment = (MyScheduleFragment) fm.findFragmentById(R.id.myScheduleFragment);
        calendarFragment = (CalendarFragment) fm.findFragmentById(R.id.calendarFragment);
        eventFragment =  (EventFragment) fm.findFragmentById(R.id.eventFragment);
        addCourseFragment = (AddCourseFragment) fm.findFragmentById(R.id.addCourseFragment);
        signoutFragment = fm.findFragmentById(R.id.userSettingsFragment);
        courseDetailFragment = (CourseDetailFragment) fm.findFragmentById(R.id.courseDetailFragment);
        eventDetailFragment = (EventDetailFragment) fm.findFragmentById(R.id.eventDetailFragment);

        hideAllFragments();    
        
        mTitle = mDrawerTitle = "Scheduloo";
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
                ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(mDrawerTitle);
                
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);


    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    
    // clear or set the flag when the activity is paused or resumed
    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        isResumed = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
        isResumed = false;
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }
    
    private Menu thisMenu = null;
    
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
     // If the nav drawer is open, hide action items related to the content view
        //boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);

        thisMenu = menu;
        menu.clear();
        
        switch(currentFrag) {
        case MYSCHEDULE:
            addNextItem();
            if (relativeScheduleDay > 0) addPreviousItem();
            break;
        case EVENT:
            addNextItem();
            if (relativeScheduleDay > 0) addPreviousItem();
            break;
        }
        
        return super.onPrepareOptionsMenu(menu);
    }
    
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        thisMenu = menu;
        return true;
    }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void addNextItem() {
        thisMenu.add(0, R.id.action_next, NEXT, R.string.action_next)
        .setIcon(R.drawable.ic_action_next_item)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }
    
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void addPreviousItem() {
        thisMenu.add(0, R.id.action_previous, PREVIOUS, R.string.action_previous)
        .setIcon(R.drawable.ic_action_previous_item)
        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        if (item.equals(menuItems[CALENDAR])) {
            showFragment(CALENDAR, true, false);
            return true;
        } else if (item.equals(menuItems[EVENT])) {
            showFragment(EVENT, true, true);
            return true;
        } else if (item.equals(menuItems[MYSCHEDULE])) {
            showFragment(MYSCHEDULE, true, true);
            return true;
        } else if (item.equals(menuItems[ADDCOURSE])) {
            showFragment(ADDCOURSE, true, true);
            return true;
        } else if (item.equals(menuItems[SIGNOUT])) {
            showFragment(SIGNOUT, true, true);
            return true;
        } else if (item.getItemId() == R.id.action_previous) {
            relativeScheduleDay--;
            showFragmentWithParams();
            return true;
        } else if (item.getItemId() == R.id.action_next) {
            relativeScheduleDay++;
            showFragmentWithParams();
            return true;
        }
        
        
        return super.onOptionsItemSelected(item);
    }

    // Interfaces implementation
    @Override
    public void onDaySelected(int d) {
        relativeScheduleDay = d;
        
        showFragment(MYSCHEDULE, true, false);

        if (relativeScheduleDay == 0)
            thisMenu.removeItem(R.id.action_previous);
        else if (thisMenu.findItem(R.id.action_previous) == null) 
            addPreviousItem();
        
        myScheduleFragment.updateView(relativeScheduleDay);
    }

    @Override
    public void onCourseSelected(int id, List<GraphUser> friends) {
        showFragment(COURSEDETAIL, true, false);
        
        courseDetailFragment.updatedFragmentView(id, userId, friends);
        
        thisMenu.removeItem(R.id.action_previous);
        thisMenu.removeItem(R.id.action_next);
    }

    @Override
    public void onEventSelected(int id, String site) {
        eventDetailFragment.updateFragmentView(id, site);
        
        showFragment(EVENTDETAIL, true, false);
        
        thisMenu.removeItem(R.id.action_previous);
        thisMenu.removeItem(R.id.action_next);
    }

    // Button Implementation
    public void addEvent(View view) {
        eventDetailFragment.addEvent(view);
    }
    
    public void findCourseLocation(View view) {
        courseDetailFragment.findCourseLocation(view);
    }
    
    public void findEventLocation(View view) {
        eventDetailFragment.findEventLocation(view);
    }
    
    public void openEventSite(View view) {
        eventDetailFragment.openEventSite(view);
    }

    public void addCourse(View view) {
        addCourseFragment.addCourse(view);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onCourseDetailLoadSelected(String title) {
        getActionBar().setTitle(title);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onEventDetailLoadSelected(String title) {
        getActionBar().setTitle(title);
    }

}