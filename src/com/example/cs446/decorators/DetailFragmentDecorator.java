package com.example.cs446.decorators;

import android.widget.ListView;

import com.example.cs446.adapters.BaseInflaterAdapter;
import com.example.cs446.adapters.inflaters.CardCourseInflater;
import com.example.cs446.interfaces.IFragment;
import com.example.cs446.models.Course;

public abstract class DetailFragmentDecorator extends IFragment {
    
    BaseInflaterAdapter<Course> adapter = new BaseInflaterAdapter<Course>(new CardCourseInflater());
    
    protected IFragment listFragment;
    
    public DetailFragmentDecorator (IFragment listFragment) {
        this.listFragment = listFragment;
    }
    
    public String loadAPIData(String url) {
        return listFragment.loadAPIData(url);
    }
    
    public void loadBackgroundTask() {
        listFragment.loadBackgroundTask();
    }
    
    public void loadUITask() {
        listFragment.loadUITask();
    }
    
    public void loadAsyncTask() {
        //new YourAsyncTask().execute();
    }
    
    public void updateView(int fragmentId) {
        loadAsyncTask();
    }
}