package com.example.cs446.decorators;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.example.cs446.adapters.BaseInflaterAdapter;
import com.example.cs446.adapters.CardItemData;
import com.example.cs446.adapters.inflaters.CardInflater;
import com.example.cs446.interfaces.IFragment;

@SuppressLint("SimpleDateFormat")
public abstract class ListFragmentDecorator extends IFragment {
    
    private SimpleDateFormat sdf = new SimpleDateFormat("EEE dd MMM");
    
    // Day relative to today's day
    protected int relDay = 0;
    
    protected static final String API_KEY = "801e2a06ff9686b4599da267caf86c7b";
    
    protected ListView list = null;
    
    protected List<CardItemData> cardList = new ArrayList<CardItemData>();
    protected BaseInflaterAdapter<CardItemData> adapter = new BaseInflaterAdapter<CardItemData>(new CardInflater());
    protected JSONArray listJSONResult = null;
    
    protected IFragment listFragment;
    
    
    public ListFragmentDecorator (IFragment listFragment) {
        this.listFragment = listFragment;
    }
    
    public String loadAPIData(String url) {
        return listFragment.loadAPIData(url);
    }
    
    public void loadBackgroundTask() {
        listFragment.loadBackgroundTask();
    }
    
    public void loadUITask() {
        listFragment.loadUITask();
        addListToAdapter();
        setActivityTitleWithDate(); 
    }
    
    public void loadAsyncTask() {
        clearCardList();
        new YourAsyncTask().execute();
    }
    
    public void updateView(int fragmentId) {
        relDay = fragmentId;
        loadAsyncTask();
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                getSelectedCard(position);
            }
         });
    }
    
    public void setActivityTitleWithDate() {
        switch(relDay) {
        case 0: getActivity().setTitle("Today"); break;
        case 1: getActivity().setTitle("Tomorrow"); break;
        default:
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, relDay);
            getActivity().setTitle(sdf.format(c.getTime()));
            break;
        }
    }
    
    public String getTitleWithDate() {
        switch(relDay) {
        case 0: return "Today";
        case 1: return "Tomorrow";
        default:
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, relDay);
            return sdf.format(c.getTime());
        }
    }
    
    protected void clearCardList() {
        adapter.clear(false);
        cardList.clear();
    }

    public void addListToAdapter() {
        for (int i = 0; i < cardList.size(); i++)
            adapter.addItem(cardList.get(i), false);
        list.setAdapter(adapter);
    }
    
    public class YourAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... args) {
            try {
                loadBackgroundTask();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        
        protected void onPostExecute(Void result) {
            loadUITask();
        }
    }
    
    
    public abstract String getAPIUrl();
    protected abstract void getSelectedCard(int position);
}